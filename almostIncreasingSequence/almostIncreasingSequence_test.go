package almostIncreasingSequence

import "testing"

func TestAlmostIncreasingSequence(t *testing.T) {
	cases := []struct {
		arr      []int
		expected bool
	}{
		{[]int{1, 3, 2, 1}, false},
		{[]int{1, 3, 2}, true},
		{[]int{1, 2, 1, 2}, false},
		{[]int{1, 4, 10, 4, 2}, false},
		{[]int{1, 2, 5, 5, 5}, false},
		{[]int{10, 1, 2, 3, 4}, true},
		{[]int{1, 2, 3, 4, 3, 6}, true},
		{[]int{1, 2, 3, 4, 99, 5, 6}, true},
		{[]int{3, 5, 67, 98, 3}, true},
	}
	for _, c := range cases {
		result := almostIncreasingSequence(c.arr)
		if result != c.expected {
			t.Errorf("Resultado fallido, esperaba %v, obtiene %v de %v", c.expected, result, c.arr)
		}
	}

}
