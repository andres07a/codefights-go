package almostIncreasingSequence

func almostIncreasingSequence(sequence []int) bool {
	removedCount := 0
	tail := 0
	for index := 1; index < len(sequence); index++ {
		if sequence[tail] < sequence[index] {
			tail = index
		} else {
			if tail == 0 {
				tail = index
			}
			if sequence[index] > sequence[tail-1] {
				tail = index
			}
			removedCount++
		}
	}
	if removedCount == 1 {
		return true
	}
	return false
}
